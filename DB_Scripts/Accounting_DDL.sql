
/*DB creation*/
USE master;
GO
	IF (NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = 'Accounting'))
		CREATE DATABASE Accounting
GO
/*Tables creation*/
USE Accounting
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*1. Customers*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customers]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Customers](
	[Id] [varchar](32) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	
	 CONSTRAINT [PK_Customers] PRIMARY KEY([Id] ASC))
	
	SET ANSI_PADDING OFF
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*2. Items*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Items]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Items](
	[Id] [varchar](32) NOT NULL,
	[Name] [varchar](70) NULL,
	
	 CONSTRAINT [PK_Items] PRIMARY KEY([Id] ASC))
	
	SET ANSI_PADDING OFF
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
/*3. PaymentMode*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaymentMode]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[PaymentMode](
	[Id] [varchar](32) NOT NULL,
	[Name] [varchar](70) NULL,
	
	 CONSTRAINT [PK_PaymentMode] PRIMARY KEY([Id] ASC))
	
	SET ANSI_PADDING OFF
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*4. Bill_Header*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Bill_Header]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Bill_Header](
	[Id] [varchar](32) NOT NULL,
	[BillDate] [datetime] NOT NULL,
	[CustomerId] [varchar](32) NOT NULL,
	[PaymentId] [varchar](32) NOT NULL,
	[Discount] [numeric](2, 2) NULL,
	 CONSTRAINT [PK_Bill_Header] PRIMARY KEY([Id] ASC))
	
	SET ANSI_PADDING OFF
	ALTER TABLE [dbo].[Bill_Header] ADD  CONSTRAINT [DF__Bill_Header__Discount]  DEFAULT ((0)) FOR [Discount]
	
	ALTER TABLE [dbo].[Bill_Header]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Header__Customers] FOREIGN KEY([CustomerId])
	REFERENCES [dbo].[Customers] ([Id])

	ALTER TABLE [dbo].[Bill_Header] CHECK CONSTRAINT [FK_Bill_Header__Customers]
	
	ALTER TABLE [dbo].[Bill_Header]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Header__PaymentMode] FOREIGN KEY([PaymentId])
	REFERENCES [dbo].[PaymentMode] ([Id])

	ALTER TABLE [dbo].[Bill_Header] CHECK CONSTRAINT [FK_Bill_Header__PaymentMode]
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

/*5. Bill_Details*/
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Bill_Details]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Bill_Details](	
	[BillId] [varchar](32) NOT NULL,
	[ItemId] [varchar](32) NOT NULL,
	[ItemPrice] [money] NOT NULL,
	[Quantity] [int] NOT NULL,	
	CONSTRAINT [PK_deleted_user] PRIMARY KEY([BillId], [ItemId]))	
	SET ANSI_PADDING OFF	

	ALTER TABLE [dbo].[Bill_Details]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Details__Bill_Header] FOREIGN KEY([BillId])
	REFERENCES [dbo].[Bill_Header] ([Id])

	ALTER TABLE [dbo].[Bill_Details] CHECK CONSTRAINT [FK_Bill_Details__Bill_Header]
	
	ALTER TABLE [dbo].[Bill_Details]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Details__Items] FOREIGN KEY([ItemId])
	REFERENCES [dbo].[Items] ([Id])

	ALTER TABLE [dbo].[Bill_Details] CHECK CONSTRAINT [FK_Bill_Details__Items]
END
GO