﻿
namespace TelerikPortal.Infrastucture
{
    public class Constants
    {
        public const string CustomerRequired = "Customer is required.";
        public const string BillDateRequired = "Bill date is required.";
        public const string PaymentMethodRequired = "Payment method is required.";        

    }
}