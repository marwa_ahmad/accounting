﻿using System.Linq;
using System.Web.Mvc;
using BLL;
using Bll.Contracts;
using Model;
using TelerikPortal.VM;

namespace TelerikPortal.Controllers
{
    public class BillController : Controller
    {
        private readonly IBillBll<Bill_Header, Bill_Details> _billBll;
        public BillController()
        {
            _billBll = new BillBll();
        }

        // GET: Bill
        public ActionResult GetAllBillsHeaders()
        {
            var billsHeaders = _billBll.GetBillHeaders();       
            return View(billsHeaders);
        }

        [HttpGet]
        public ActionResult CreateHeader()
        {
            var customers = new CustomerBll().GetAll();
            var payments = new PaymentBll().GetAll();

            var billHeaderCVM = new BillHeaderCVM();
            if (customers != null)
            {                
                var customersList = customers.Select(f => new SelectListItem {Value = f.Id,Text = f.Name});
                billHeaderCVM.Customers = customersList;
            }
            if (payments != null)
            {
                var paymentsList = payments.Select(f => new SelectListItem { Value = f.Id, Text = f.Name });
                billHeaderCVM.Payments = paymentsList;
            }
            return View(billHeaderCVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateHeader(BillHeaderCVM billHeaderModel)
        {
            if (ModelState.IsValid == false) return View(billHeaderModel);
            var billHeader = new Bill_Header()
            {                
                CustomerId = billHeaderModel.SelectedCustomerId,
                Discount = billHeaderModel.Discount,
                PaymentId = billHeaderModel.SelectedPaymentId
            };
            _billBll.AddBillHeader(billHeader);
            return RedirectToAction("GetAllBillsHeaders");
        }

        public ActionResult GetAllBillDetails(string billId)
        {
            var billHeader = _billBll.GetBillHeader(billId);
            return billHeader != null ? View(billHeader.Bill_Details) : View();
        }
    }
}