﻿using System.Web.Mvc;
using BLL;
using Bll.Contracts;
using Model;

namespace TelerikPortal.Controllers
{
    public class PaymentModeController : Controller
    {
        private readonly IPaymentBll<PaymentMode> _paymentBll;
        public PaymentModeController()
        {
            _paymentBll = new PaymentBll();
        }

        // GET: PaymentMode
        public ActionResult GetAll()
        {
            var orderdPaymentModes = _paymentBll.GetAll();
            return View(orderdPaymentModes);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PaymentMode modelPaymentMode)
        {
            if (ModelState.IsValid == false) return View(modelPaymentMode);

            _paymentBll.AddPayment(modelPaymentMode);
            return RedirectToAction("GetAll");
        }
    }
}