﻿using System.Linq;
using System.Web.Mvc;
using BLL;
using Bll.Contracts;
using Model;

namespace TelerikPortal.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerBll<Customer> _customerBll;
        public CustomerController()
        {
            _customerBll = new CustomerBll();
        }

        // GET: Customer
        public ActionResult GetAll()
        {
            var customers = _customerBll.GetAll();
            return View(customers);
        }

        [HttpGet]
        public JsonResult GetAllJson()
        {
            var customers = _customerBll.GetAll();
            return Json(customers.Select(c => new { Name = c.Name, ID = c.Id }), JsonRequestBehavior.AllowGet);            
        }

        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Customer modelCustomer)
        {
            if (ModelState.IsValid == false) return View(modelCustomer);
            _customerBll.AddCustomer(modelCustomer);
            return RedirectToAction("GetAll");
        }
    }
}