﻿using System.Web.Mvc;
using BLL;
using Bll.Contracts;
using Model;

namespace TelerikPortal.Controllers
{
    public class ItemController : Controller
    {
        private readonly IItemBll<Item> _itemBll;
        public ItemController()
        {
            _itemBll = new ItemBll();
        }
        
        public ActionResult GetAll()
        {
            var orderdItems = _itemBll.GetAll();
            return View(orderdItems);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Item modelItem)
        {
            if (ModelState.IsValid == false) return View(modelItem);

            _itemBll.AddItem(modelItem);
            return RedirectToAction("GetAll");
        }
    }
}