﻿using System.Web.Optimization;

namespace TelerikPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryvalidate").Include(
            "~/Scripts/jquery.validate-vsdoc.js",
            "~/Scripts/jquery.validate.min.js",
            "~/Scripts/jquery.validate.unobtrusive.min.js"));

            //kendo
            bundles.Add(new ScriptBundle("~/bundles/kendo/main").Include(
                        "~/Scripts/kendo/2015.3.1111/jquery.min.js",
                        "~/Scripts/kendo/2015.3.1111/jszip.min.js",
                        "~/Scripts/kendo/2015.3.1111/kendo.all.min.js",
                        "~/Scripts/kendo/2015.3.1111/kendo.aspnetmvc.min.js",
                        "~/Scripts/kendo.modernizr.custom.js",
                        "~/Scripts/kendo/2015.3.1111/kendo*"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/kendo/all").Include(
            "~/Scripts/kendo/2015.3.1111/kendo*"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
