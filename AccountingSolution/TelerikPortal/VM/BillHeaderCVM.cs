﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using TelerikPortal.Infrastucture;

namespace TelerikPortal.VM
{
    public class BillHeaderCVM
    {
        public IEnumerable<SelectListItem> Customers { get; set; }

        [Required(ErrorMessage = Constants.CustomerRequired)]
        [Display(Name = "Customer")]
        public string SelectedCustomerId { get; set; }

        public IEnumerable<SelectListItem> Payments { get; set; }

        [Required(ErrorMessage = Constants.PaymentMethodRequired)]
        [Display(Name = "Payment Method")]
        public string SelectedPaymentId { get; set; }


        [Range(0, 100)]
        public int Discount { get; set; }
    }
}