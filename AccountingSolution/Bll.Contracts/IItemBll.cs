﻿using System.Collections.Generic;

namespace Bll.Contracts
{
    public interface IItemBll<T>
    {
        void AddItem(T item);
        List<T> GetAll();
    }
}