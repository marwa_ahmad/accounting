﻿using System.Collections.Generic;

namespace Bll.Contracts
{
    public interface IPaymentBll<T>
    {
        void AddPayment(T paymentMode);
        List<T> GetAll();
    }
}