﻿using System.Collections.Generic;

namespace Bll.Contracts
{
    public interface ICustomerBll<T>
    {
        void AddCustomer(T customer);
        List<T> GetAll();
    }
}