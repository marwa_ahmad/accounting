﻿using System.Collections.Generic;

namespace Bll.Contracts
{
    public interface IBillBll<THeader, TDetails>
    {
        void AddBill(THeader billHeader, TDetails billDetails);
        void AddBillHeader(THeader billHeader);
        List<THeader> GetBillHeaders();
        THeader GetBillHeader(string billId);
    }
}