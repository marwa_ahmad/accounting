﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bll.Contracts;
using DAL;
using Model;
using Common;

namespace BLL
{
    public class ItemBll : IItemBll<Item>
    {
        private readonly AccountingEntities _accountingDbContext;
        public ItemBll()
        {
            _accountingDbContext = new AccountingEntities();
        }

        public void AddItem(Item item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            _accountingDbContext.Items.Add(item);
            _accountingDbContext.SaveChanges(); 
        }

        public List<Item> GetAll()
        {
            var orderdItems = from c in _accountingDbContext.Items
                                     orderby c.Name
                                     select c;
            return orderdItems.IsNullOrEmpty() ? null : orderdItems.ToList();
        }
    }
}
