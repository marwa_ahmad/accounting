﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bll.Contracts;
using DAL;
using Model;
using Common;

namespace BLL
{
    public class PaymentBll : IPaymentBll<PaymentMode>
    {
        private readonly AccountingEntities _accountingDbContext;
        public PaymentBll()
        {
            _accountingDbContext = new AccountingEntities();
        }

        public void AddPayment(PaymentMode paymentMode)
        {
            paymentMode.Id = Guid.NewGuid().ToString("N");
            _accountingDbContext.PaymentModes.Add(paymentMode);
            _accountingDbContext.SaveChanges(); 
        }

        public List<PaymentMode> GetAll()
        {
            var orderdPaymentModes = from pm in _accountingDbContext.PaymentModes
                                     orderby pm.Name
                                     select pm;
            return orderdPaymentModes.IsNullOrEmpty() ? null : orderdPaymentModes.ToList();
        }
    }
}
