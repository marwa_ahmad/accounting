﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bll.Contracts;
using DAL;
using Model;
using Common;

namespace BLL
{
    public class BillBll : IBillBll<Bill_Header, Bill_Details>
    {
        private readonly AccountingEntities _accountingDbContext;
        public BillBll()
        {
            _accountingDbContext = new AccountingEntities();
        }

        public void AddBill(Bill_Header billHeader, Bill_Details billDetails)
        {
            billHeader.Id = Guid.NewGuid().ToString("N");
            billHeader.BillDate = DateTime.UtcNow;
            billDetails.BillId = billHeader.Id;
            _accountingDbContext.Bill_Header.Add(billHeader);
            if(billDetails != null) _accountingDbContext.Bill_Details.Add(billDetails);

            _accountingDbContext.SaveChanges(); 
        }

        public void AddBillHeader(Bill_Header billHeader)
        {
            billHeader.Id = Guid.NewGuid().ToString("N");
            billHeader.BillDate = DateTime.UtcNow;
            _accountingDbContext.Bill_Header.Add(billHeader);            

            _accountingDbContext.SaveChanges(); 
        }

        public List<Bill_Header> GetBillHeaders()
        {
            var orderedBills = from b in _accountingDbContext.Bill_Header
                                     orderby b.BillDate descending 
                                     select b;
            return orderedBills.IsNullOrEmpty() ? null : orderedBills.ToList();
        }

        public Bill_Header GetBillHeader(string billId)
        {
            var bill = from b in _accountingDbContext.Bill_Header
                               where string.CompareOrdinal(billId, b.Id) == 0
                               select b;
            return bill.IsNullOrEmpty() ? null : bill.FirstOrDefault();
        }
    }
}
