﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bll.Contracts;
using DAL;
using Model;
using Common;

namespace BLL
{
    public class CustomerBll : ICustomerBll<Customer>
    {
        private readonly AccountingEntities _accountingDbContext;
        public CustomerBll()
        {
            _accountingDbContext = new AccountingEntities();
        }

        public void AddCustomer(Customer customer)
        {
            customer.Id = Guid.NewGuid().ToString("N");
            _accountingDbContext.Customers.Add(customer);
            _accountingDbContext.SaveChanges(); 
        }

        public List<Customer> GetAll()
        {
            var orderedCustomers = from c in _accountingDbContext.Customers
                                     orderby c.Name
                                     select c;
            return orderedCustomers.IsNullOrEmpty() ? null : orderedCustomers.ToList();
        }
    }
}
