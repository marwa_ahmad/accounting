﻿using System.Collections.Generic;
using System.Linq;

namespace Common
{
    public static class ListExtension
    {
        public static bool IsNullOrEmpty<T>(this IQueryable<T> collection)
        {
            return collection == null || collection.Any() == false;
        }
        public static bool NotEmpty<T>(this IList<T> list)
        {
            return list != null && list.Count > 0;
        }
    }
}
